"""
Author: matth
Date Created: 13/02/2017 11:05 AM
"""

from __future__ import division
# below LOD are negative


mav = {
'ecoli': -1,
    'NO3_N': 11.3,
    'Mn': 0.4,
    'As': 0.01,
}

gv = {
    'NH4_N': 1.2,
    'Cl' : 250,
    'hardness': 200,
    'Fe': 0.2,
    'Mn': 0.04,
    'pH_lab': (7.0, 8.5),
    'Na': 200,
    'SO4': 250
}

if __name__ == '__main__':
    print('test')