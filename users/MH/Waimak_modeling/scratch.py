from __future__ import division
import numpy as np
import pandas as pd
import users.MH.Waimak_modeling.model_tools as mt
import flopy
import glob
import matplotlib.pyplot as plt
from users.MH.Waimak_modeling.supporting_data_path import base_mod_dir
import os
from core.ecan_io import rd_sql, sql_db


keys = ['eyre_swaz_sd',
        'custmain_swaz_sd',
        'ashley_swaz_sd',
        'num7drn_swaz_sd',
        'custr_swaz_sd',
        'taranaki_swaz',
        'courtenay_swaz',
        'kairaki_swaz',
        'northbrook_swaz',
        'kaiapoi_swaz',
        'greigs_swaz',
        'southbrook_swaz',
        'ohoka_swaz',
        'cam_swaz',
        'waikuku_swaz',
        'saltwater_swaz']
vals=[150,30,7]
for val in vals:
    outdata = pd.DataFrame(index=['min', 'max', 'mean'],columns=keys)
    cust = pd.read_csv(r"P:\Groundwater\Waimakariri\Groundwater\Numerical GW model\Model simulations and results\stream_depletion\stream_depletion_{v}\stream_d{v}_data_1cu_cust_joined.csv".format(v=val))
    org = pd.read_csv(r"P:\Groundwater\Waimakariri\Groundwater\Numerical GW model\Model simulations and results\stream_depletion\stream_depletion_{v}\stream_d{v}_data_joined.csv".format(v=val))

    for key in keys:
        outdata.loc['max',key] = (org[key] - cust[key]).max()
        outdata.loc['min', key] = (org[key] - cust[key]).min()
        outdata.loc['mean', key] = (org[key] - cust[key]).mean()
    outdata.to_csv(r"P:\Groundwater\Waimakariri\Groundwater\Numerical GW model\Model simulations and results\stream_depletion\stream_depletion_{v}\comparison_sd{v}_org_1cu_cust.csv".format(v=val))

