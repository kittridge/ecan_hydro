"""
Author: matth
Date Created: 23/05/2017 1:48 PM
"""

from __future__ import division
from core import env
from m_wraps.base_modflow_wrapper import get_base_mf_ss
import pickle
import os
from users.MH.Waimak_modeling.supporting_data_path import sdp
from polygon_to_model_array import shape_file_to_model_array
import numpy as np
import pandas as pd

def get_base_str (recalc=False):
    picklepath = '{}/inputs/pickeled_files/base_stream_spd.p'.format(sdp)

    if os.path.exists(picklepath) and not recalc:
        base_str = pickle.load(open(picklepath))
        return base_str

    org_m = get_base_mf_ss()
    base_str = org_m.str.stress_period_data.data[0]
    pickle.dump(base_str,open(picklepath,'w'))

    return base_str

def get_base_seg_data(recalc=False):
    picklepath = '{}/inputs/pickeled_files/base_stream_segments.p'.format(sdp)

    if os.path.exists(picklepath) and not recalc:
        base_str = pickle.load(open(picklepath))
        return base_str

    org_m = get_base_mf_ss()
    base_str = org_m.str.segment_data[0]
    pickle.dump(base_str,open(picklepath,'w'))

    return base_str


def get_base_rch(recalc=False):
    picklepath = '{}/inputs/pickeled_files/base_recharge_spd.p'.format(sdp)

    if os.path.exists(picklepath) and not recalc:
        base_rch = pickle.load(open(picklepath))
        return base_rch

    org_m = get_base_mf_ss()
    base_rch = org_m.rch.rech.array[0,0]
    pickle.dump(base_rch, open(picklepath, 'w'))

    return base_rch


def get_gmp_rch(recalc=False):
    picklepath = '{}/inputs/pickeled_files/gmp_recharge_spd.p'.format(sdp)

    if os.path.exists(picklepath) and not recalc:
        gmp_rch = pickle.load(open(picklepath))
        return gmp_rch

    base_rch = get_base_rch()

    factor = shape_file_to_model_array("{}/inputs/shp_files/rch_s/gmp_to_cmp.shp".format(sdp),
                                       'OMPLSRFact', alltouched=True)
    factor[np.isnan(factor)] = 1
    factor[~np.isclose(factor,1)] *= 1.3 # this increase is to deal with the differences between fouad's Cmp layer and all other cmp layers

    gmp_rch = base_rch/factor
    pickle.dump(gmp_rch,open(picklepath,'w'))

    return gmp_rch

def get_true_cmp_rch(recalc=False):
    picklepath = '{}/inputs/pickeled_files/true_cmp_recharge_spd.p'.format(sdp)

    if os.path.exists(picklepath) and not recalc:
        true_cmp_rch = pickle.load(open(picklepath))
        return true_cmp_rch

    # the current model has something that is probably closer to gmp than cmp, so here we back calculate a more
    # realistic cmp layer.  the naming true CMP is to not kill my naming.
    base_rch = get_base_rch()

    factor = shape_file_to_model_array("{}/inputs/shp_files/rch_s/gmp_to_cmp.shp".format(sdp),
                                       'OMPLSRFact', alltouched=True)
    factor[np.isnan(factor)] = 1

    true_cmp_rch = base_rch * factor
    pickle.dump(true_cmp_rch,open(picklepath,'w'))
    return true_cmp_rch



def get_nat_rch(recalc=False):
    picklepath = '{}/inputs/pickeled_files/nat_recharge_spd.p'.format(sdp)

    if os.path.exists(picklepath) and not recalc:
        nat_rch = pickle.load(open(picklepath))
        return nat_rch

    base_rch = get_base_rch()
    nat_rch = shape_file_to_model_array("{}/inputs/shp_files/rch_s/drylandlsr_nodes_0.shp".format(sdp),
                                        'Topography', alltouched=True)

    nat_rch[nat_rch<=0] = np.nan
    nat_rch *= 2.73973e-6
    idx = np.isnan(nat_rch)
    nat_rch[idx] = base_rch[idx]
    pickle.dump(nat_rch,open(picklepath,'w'))
    return nat_rch

def get_stream_duplication_array(recalc=False):
    #todo it looks like things are not working perfectly with this... really tease this out for N
    picklepath = '{}/inputs/pickeled_files/str_dup_array.p'.format(sdp)

    if os.path.exists(picklepath) and not recalc:
        str_dup_array = pickle.load(open(picklepath))
        return str_dup_array

    base_str = pd.DataFrame(get_base_str())
    str_dup_array = np.zeros((190,365))
    for i in base_str.index:
        row, col = base_str.loc[i,['i','j']]
        str_dup_array[row, col] +=1

    pickle.dump(str_dup_array,open(picklepath,'w'))
    return str_dup_array


if __name__ == '__main__':
    base = get_base_rch()
    gmp = get_gmp_rch(True)
    print((base*40000-gmp*40000).sum()/86400)
