"""
Author: matth
Date Created: 14/06/2017 12:27 PM
"""

from __future__ import division
from core import env
import numpy as np
import pandas as pd
import flopy
from users.MH.Waimak_modeling.model_tools.get_str_rch_values import get_base_str, get_base_seg_data
from users.MH.Waimak_modeling.model_tools.drain_concentration import get_drn_samp_pts_dict
from users.MH.Waimak_modeling.model_tools.polygon_to_model_array import shape_file_to_model_array
from users.MH.Waimak_modeling.supporting_data_path import sdp
import geopandas as gpd
from copy import deepcopy


def create_sfr_package(m, version=1, seg_v=1, reach_v=1):
    """
    wrapper to create sfr packages
    :param m: the model on which to create the package
    :param version: which sfr version to use.  implemented here to record multiple attempts at the sfr
    :param seg_v: which version of the segment data to use here to record multiple attempts at the sfr
    :param reach_v: which verson of the reach data to use here to record multiple attempts at the sfr
    :return:
    """
    if version == 1:
        _create_sfr_version_1(m, seg_v, reach_v)


def _create_sfr_version_1(m, seg_v, reach_v):
    """
    sfr version one, no transient flow routing, no unsaturated zone flow
    :param m: modflow model object on which to build the package
    :return:
    """
    reach_data = _get_reach_data(reach_v)
    segment_data = _get_segment_data(seg_v)
    sfr = flopy.modflow.ModflowSfr2(m,
                                    nstrm=len(reach_data),
                                    nss=len(segment_data),
                                    nsfrpar=0,  # this is not implemented in flopy not using
                                    nparseg=0,  # this is not implemented in flopy not using
                                    const=86400,
                                    dleak=0.0001,  # default, likely ok
                                    ipakcb=740,
                                    istcb2=740,  # todo check output
                                    isfropt=1,  # Not using as nstrm >0 therfore no Unsaturated zone flow
                                    nstrail=10,  # not using as no unsaturated zone flow
                                    isuzn=1,  # not using as no unsaturated zone flow
                                    nsfrsets=30,  # not using as no unsaturated zone flow
                                    irtflg=0,  # no transient sf routing
                                    numtim=2,  # Not using transient SFR
                                    weight=0.75,  # not using if irtflg = 0 (no transient SFR)
                                    flwtol=0.0001,  # not using if irtflg = 0 (no transient SFR)
                                    reach_data=reach_data,
                                    segment_data=segment_data,
                                    channel_geometry_data=None,  # using rectangular profile so not used
                                    channel_flow_data=None,  # icalc = 1 or 2 so not using
                                    dataset_5=None,  # todo
                                    reachinput=True,  # using for reach input
                                    transroute=False,  # no transient sf routing
                                    tabfiles=False,  # not using
                                    tabfiles_dict=None,  # not using
                                    unit_number=717)


def _get_segment_data(seg_v):
    """
    wrapper to get the segment data
    :param seg_v: the version here to record multiple attempts at the sfr
    :return:
    """
    #todo add pickle option if slow
    if seg_v == 1:
        seg_data = _seg_data_v1()
    else:
        raise ValueError('unexpected version for segment: {}'.format(seg_v))
    return seg_data


def _get_reach_data(reach_v):
    """
    wrapper to get the reach data
    :param reach_v: the version to use here to record multiple attempts at the sfr
    :return:
    """
    #todo add pickle option if slow
    if reach_v == 1:
        reach_data = _reach_data_v1()
    else:
        raise ValueError('unexpected version for reach: {}'.format(reach_v))
    return reach_data


def _reach_data_v1():
    temp_str_data = _get_base_stream_values()
    outdata = flopy.modflow.ModflowSfr2.get_empty_reach_data(len(temp_str_data.index))
    data_to_pass = {'i': 'i', 'j': 'j', 'k': 'k', 'reach': 'ireach', 'segment': 'iseg'}
    for key in data_to_pass.keys():
        outdata[data_to_pass[key]] = temp_str_data[key]

    outdata = _define_reach_length(outdata, mode='cornering')
    #todo strtop work with DEM to fix up streams so they are not too subterranian
    # could define a incision rate for each segment (or interpolation between two ends)
    # how much of a problem is a subterrainian stream in teh big picture
    #todo slope
    #todo strthick
    #todo k


    # todo node is negative and other not used values is this a problem think about default values
    return outdata

def _seg_data_v1():
    seg_data = flopy.modflow.ModflowSfr2.get_empty_segment_data(44, default_value=0)
    seg_data['nseg'] = range(1, 45)
    seg_data['icalc'] = 1

    str_data = _get_base_stream_values()

    # outseg
    org_seg_data = pd.DataFrame(get_base_seg_data())
    org_seg_data['seg'] = range(1,len(org_seg_data)+1)
    trib_dict = dict(np.array(org_seg_data[['itrib01','seg']]))
    trib_dict.update(np.array(org_seg_data[['itrib02','seg']]))
    trib_dict.update(np.array(org_seg_data[['itrib03','seg']]))
    for i,seg in enumerate(seg_data['nseg']):
        if seg in trib_dict.keys():
            seg_data['outseg'][i] = trib_dict[seg]
    # get flows
    for i,seg in enumerate(seg_data['nseg']):
        seg_data['flow'][i] = str_data['flow'][(str_data['reach']==1) & (str_data['segment']==seg)].iloc[0]


    # hard variable
    # todo width 1/2



    raise NotImplementedError()  # todo


def _get_base_stream_values():
    """
    get all current data and locations for the stream segments.  it will be a mix of drains and stream and made up data
    :return:
    """
    # get rid of duplicates from the orginal stream package
    str_data = pd.DataFrame(get_base_str())
    dup = str_data.duplicated(['i', 'j'], False)
    str_data = str_data[(~dup) | (str_data['reach'] == 1)]

    # add drain cells and additional cells that we need
    drn_data = gpd.read_file("{}/inputs/shp_files/num7_drn_to_str/drn_to_str.shp".format(sdp))
    drn_data = drn_data[['elv', 'lat', 'lon', 'reach', 'xdata', 'ydata']]
    drn_data = drn_data.rename(columns={'xdata': 'j', 'ydata': 'i'})

    # add other link cells
    idx = len(drn_data.index)
    for i, row in enumerate(range(64, 69)):
        drn_data.loc[idx + i, ['i', 'j', 'reach']] = (row, 277, idx + i + 1)
    drn_data['segment'] = 27
    drn_data['k'] = 0

    # combine drn cells and str_data #todo do I need to split up the segment in the num7 drain?
    #todo do I want all of the the drain cells
    str_data['reach'][str_data['segment'] == 27] += drn_data['reach'].max()
    str_data = pd.concat((drn_data, str_data))
    str_data = str_data.sort_values(['segment', 'reach'])

    return str_data


def _define_reach_length(reach_data, mode='cornering'):
    """
    define the reach length for the SFR package
    :param reach_data: dataframe of reach data
    :param mode: the mode to use to determine reach length options:
                 constant: use the length of the grid cell (e.g. 200 m)
                 cornering: use the length of the grid cell for straight segments and use hypotenuse (of the half cell)
                            for corner cells assume kitty corner to kitty corner cells move straight across the cell
                            (e.g. the hypotius of the full cell) for kitty corner to adjacent assume that the stream
                            goes to teh center of the cell (along hypotenus of the half cell) and then straigth out to
                            the next cell (half cell lenght)

    :return: rech_data with length filled in
    """
    wrd = deepcopy(reach_data)
    cell_dim = 200
    if mode == 'constant':
        wrd['rchlen'] = cell_dim
    elif mode == 'cornering':
        rchlen = np.zeros(len(wrd['rchlen']))
        for i, rch, seg in zip(range(0, len(rchlen)), wrd['ireach'], wrd['iseg']):
            if rch == 1:  # assume full length for the first segment of each reach
                rchlen[i] = cell_dim
                continue
            if rch == wrd['ireach'][wrd['iseg'] == seg].max():  # assume full length for the last reach of the segment
                rchlen[i] = cell_dim
                continue

            # for all middle reaches check to see if it is a corner and then assign values associated with that
            c_row, c_col, c_seg, c_rch = wrd[['j', 'i', 'iseg', 'ireach']][i]
            p_row, p_col, p_seg, p_rch = wrd[['j', 'i', 'iseg', 'ireach']][i - 1]
            n_row, n_col, n_seg, n_rch = wrd[['j', 'i', 'iseg', 'ireach']][i + 1]

            # some checks to make sure everything is working right
            if p_seg != seg or n_seg != seg:
                raise ValueError('different segments adjacent for item: {}, reach: {}, seg: {} '.format(i, rch, seg))
            elif n_rch != rch + 1:
                raise ValueError('unexpected reaches adjacent for item: {}, reach: {}, seg: {} '.format(i, rch, seg))
            elif p_rch != rch - 1:
                raise ValueError('unexpected reaches adjacent for item: {}, reach: {}, seg: {} '.format(i, rch, seg))
            elif not np.in1d([p_row,n_row], range(c_row-1,c_row+2)).all():
                raise ValueError('rows not adjacent for item: {}, reach: {}, seg: {} '.format(i, rch, seg))
            elif not np.in1d([p_col,n_col], range(c_col-1,c_col+2)).all():
                raise ValueError('cols not adjacent for item: {}, reach: {}, seg: {} '.format(i, rch, seg))

            # different options for based on the location of the previous and next reach
            if p_row==c_row==n_row:  # straight segment across rows
                rchlen[i] = cell_dim
            elif p_col==c_col==n_col:  # straight segment across columns
                rchlen[i] = cell_dim
            elif p_col == c_col:
                if c_row == n_row: # assume the river cuts the corner
                    rchlen[i] = ((cell_dim/2)**2 + (cell_dim/2)**2)**0.5
                else: # assume the river goes to the centre of the cell and out
                    rchlen[i] = ((cell_dim)**2 + (cell_dim)**2) ** 0.5 + cell_dim/2
            elif p_row == c_row:
                if c_col == n_col: # assume the river cuts the corner
                    rchlen[i] = ((cell_dim / 2)**2 + (cell_dim / 2)**2) ** 0.5
                else: # assume the river goes to the centre of the cell and out
                    rchlen[i] = ((cell_dim)**2 + (cell_dim)**2) ** 0.5 + cell_dim/2
            else:
                if c_row == n_row or c_col == n_col: # assume the river goes to the centre of the cell and out
                    rchlen[i] = ((cell_dim / 2)**2 + (cell_dim / 2)**2) ** 0.5
                else: # assume the river cuts acorss the entire cell linearly
                    rchlen[i] = ((cell_dim)**2 + (cell_dim)**2) ** 0.5 + cell_dim/2
        wrd['rchlen'] = rchlen
    else:
        raise ValueError('unexpected argurment for mode: {}'.format(mode))
    return wrd

if __name__ == '__main__':
    test = _reach_data_v1()
    print test.keys()
