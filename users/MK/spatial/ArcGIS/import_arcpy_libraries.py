# -*- coding: utf-8 -*-
"""
The necessary paths to libraries to be able to import arcpy.
"""

import sys
sys.path.append("C:\\Python27\\ArcGIS10.4\\Lib\\site-packages")
sys.path.append(r'C:\Program Files (x86)\ArcGIS\Desktop10.4\arcpy')
sys.path.append(r'C:\Program Files (x86)\ArcGIS\Desktop10.4\ArcToolbox\Scripts')
sys.path.append(r'C:\Program Files (x86)\ArcGIS\Desktop10.4\bin')
sys.path.append("C:\\Python27\\ArcGIS10.4\\lib")

import arcpy
